

<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <?php if(Auth::user()->user_role == "Student"): ?>
            <h1>My History Book Loan</h1>
        <?php else: ?>
            <h1>List Book Loan</h1>
        <?php endif; ?>
    </div>
    <?php if(Auth::user()->user_role != "Student"): ?>
        <div class="row justify-content-center">
            <a href="<?php echo e(route('rent_book.create')); ?>" class="btn btn-primary pull-right">Add New</a>
            <br><br><br>
        </div>
    <?php endif; ?>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <table id="tablee" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Book</th>
                        <?php if(Auth::user()->user_role != "Student"): ?>
                            <th>Student</th>
                        <?php endif; ?>
                        <th>Duration</th>
                        <th>Date Start</th>
                        <?php if(Auth::user()->user_role != "Student"): ?>
                            <th>Date End</th>
                            <th>Status</th>
                            <th>Staff</th>
                            <th>Action</th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $rent_books; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($loop->iteration); ?></td>
                            <td><?php echo e($row->book_name); ?></td>
                            <?php if(Auth::user()->user_role != "Student"): ?>
                                <td><?php echo e($students[$row->student_id]); ?></td>
                            <?php endif; ?>
                            <td><?php echo e($row->rent_book_duration); ?> Day</td>
                            <td><?php echo e(date('l, d F Y', strtotime($row->rent_book_date_start))); ?></td>
                            <?php if(Auth::user()->user_role != "Student"): ?>
                                <td>
                                    <?php if($row->rent_book_date_finish == ""): ?>
                                        <b>Estimated</b> <?php echo e(date('l, d F Y', strtotime($row->rent_book_date_start) + ($row->rent_book_duration * 86400))); ?>

                                        <?php if($row->rent_book_status == "On Loan"): ?>
                                            <br><a href="<?php echo e(url('rent_book/finish/' . $row->rent_book_id)); ?>" class="btn btn-warning">Finish now!</a>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <?php echo e(date('l, d F Y', strtotime($row->rent_book_date_finish))); ?>

                                    <?php endif; ?>
                                </td>
                                <td><?php echo e($row->rent_book_status); ?></td>
                                <td><?php echo e($library_staffs[$row->library_staff_id]); ?></td>
                                <td>
                                    <?php if($row->rent_book_date_finish == ""): ?>
                                        <form method="POST" action="<?php echo e(route('rent_book.destroy', $row->rent_book_id)); ?>">
                                            <?php echo e(method_field('delete')); ?>

                                            <?php echo e(csrf_field()); ?>

                                            <div class="actions">
                                                <a class="btn btn-primary" href="<?php echo e(route('rent_book.edit', $row->rent_book_id)); ?>" aria-label="Edit">
                                                    Edit
                                                </a>
                                                <button class="btn btn-danger" type="submit" name="delete" aria-label="Delete">
                                                    Delete
                                                </button>
                                            </div>
                                        </form>
                                    <?php else: ?>
                                        -
                                    <?php endif; ?>
                                </td>
                            <?php endif; ?>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\_cns_sekolah\resources\views/rent_book/index.blade.php ENDPATH**/ ?>