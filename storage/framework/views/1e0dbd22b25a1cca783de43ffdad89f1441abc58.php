<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <h1>Class <?php echo e($classs->class_name); ?> - Choose Course</h1>
        <div class="col-md-12">
            <table id="tablee" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($row->course_name); ?></td>
                            <td>
                                <a class="btn btn-primary" href="<?php echo e(url('valuation/input/' . $classs->class_id . '/' . $row->course_id)); ?>" aria-label="Edit">
                                    Input
                                </a>
                                <a class="btn btn-success" href="<?php echo e(url('valuation/report/' . $classs->class_id . '/' . $row->course_id)); ?>" aria-label="Edit">
                                    Report
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            <a href="<?php echo e(url('valuation/pre/index/')); ?>" class="btn btn-danger">Back</a>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\_cns_sekolah\resources\views/valuation/pre_index_course.blade.php ENDPATH**/ ?>