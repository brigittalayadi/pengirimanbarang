<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>

    <!-- Scripts -->
    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" >
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" >
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" >
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light bg-dark">
            <div class="container">
                <a class="navbar-brand text-light" href="<?php echo e(url('/')); ?>">
                    <?php echo e(config('app.name', 'Laravel')); ?>

                </a>

                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav ml-auto">
                        <?php if(auth()->guard()->guest()): ?>
                        <?php else: ?>
                            <?php if(Gate::check('admin')): ?>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Master
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="<?php echo e(route('student.index')); ?>">Student</a>
                                        <a class="dropdown-item" href="<?php echo e(route('teacher.index')); ?>">Teacher</a>
                                        <a class="dropdown-item" href="<?php echo e(route('library_staff.index')); ?>">Library Staff</a>
                                        <a class="dropdown-item" href="<?php echo e(route('headmaster.index')); ?>">Headmaster</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="<?php echo e(route('book.index')); ?>">Book</a>
                                        <a class="dropdown-item" href="<?php echo e(route('class.index')); ?>">Class</a>
                                        <a class="dropdown-item" href="<?php echo e(route('course.index')); ?>">Course</a>
                                    </div>
                                </li>
                            <?php endif; ?>

                            <?php if(Gate::check('admin') || Gate::check('library_staff') || Gate::check('student')): ?>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Library
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <?php if(Gate::check('student')): ?>
                                            <a class="dropdown-item" href="<?php echo e(route('rent_book.show', encrypt(Auth::user()->user_id))); ?>">My History Book Loan</a>
                                        <?php endif; ?>
                                        <?php if(Gate::check('admin') || Gate::check('library_staff')): ?>
                                            <a class="dropdown-item" href="<?php echo e(route('rent_book.index')); ?>">Book Loan</a>
                                        <?php endif; ?>
                                    </div>
                                </li>
                            <?php endif; ?>

                            <?php if(Gate::check('admin') || Gate::check('teacher') || Gate::check('student')): ?>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Study Result
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <?php if(Gate::check('student')): ?>
                                            <a class="dropdown-item" href="<?php echo e(url('valuation/student/report/' . encrypt(Auth::user()->user_id))); ?>">My Study Card</a>
                                        <?php endif; ?>
                                        <?php if(Gate::check('admin') || Gate::check('teacher')): ?>
                                            <a class="dropdown-item" href="<?php echo e(url('valuation/pre/index_student')); ?>">Student's Study Card</a>
                                            <a class="dropdown-item" href="<?php echo e(url('valuation/pre/index')); ?>">Input or Report</a>
                                        <?php endif; ?>
                                    </div>
                                </li>
                            <?php endif; ?>

                            <li class="nav-item">
                                <a class="nav-link text-danger" href="<?php echo e(route('logout')); ?>"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    <?php echo e(__('Logout')); ?>

                                </a>

                                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                    <?php echo csrf_field(); ?>
                                </form>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <?php echo $__env->yieldContent('content'); ?>
        </main>
    </div>
</body>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#tablee').DataTable({
          'responsive'  : false,
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true,
        });
    });   
</script>

</html><?php /**PATH C:\xampp\htdocs\_cns_sekolah\resources\views/layouts/app.blade.php ENDPATH**/ ?>