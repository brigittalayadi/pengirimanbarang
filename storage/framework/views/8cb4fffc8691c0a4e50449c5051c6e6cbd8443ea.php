<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <h1>Class <?php echo e($classs->class_name); ?> - Course <?php echo e($courses->course_name); ?></h1>
        <div class="col-md-12">
            <table id="tablee" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Grade</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($loop->iteration); ?></td>
                            <td><?php echo e($row->user_name); ?></td>
                            <td><?php echo e($valuations[$row->user_id]); ?></td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\cns_sekolah\resources\views/valuation/report_valuation.blade.php ENDPATH**/ ?>