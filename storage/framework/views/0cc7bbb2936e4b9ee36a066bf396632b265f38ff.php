<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <h1>List Teacher</h1>
    </div>
    <div class="row justify-content-center">
        <a href="<?php echo e(route('teacher.create')); ?>" class="btn btn-primary pull-right">Add New</a>
        <br><br><br>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <table id="tablee" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>NIP</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($loop->iteration); ?></td>
                            <td><?php echo e($row->user_nip); ?></td>
                            <td><?php echo e($row->user_name); ?></td>
                            <td><?php echo e($row->email); ?></td>
                            <td>
                                <form method="POST" action="<?php echo e(route('teacher.destroy', $row->user_id)); ?>">
                                    <?php echo e(method_field('delete')); ?>

                                    <?php echo e(csrf_field()); ?>

                                    <div class="actions">
                                        <a class="btn btn-primary" href="<?php echo e(route('teacher.edit', $row->user_id)); ?>" aria-label="Edit">
                                            Edit
                                        </a>
                                        <button class="btn btn-danger" type="submit" name="delete" aria-label="Delete">
                                            Delete
                                        </button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\cns_sekolah\resources\views/teacher/index.blade.php ENDPATH**/ ?>