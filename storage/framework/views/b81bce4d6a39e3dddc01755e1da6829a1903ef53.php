<?php $__env->startSection('content'); ?>
<div class="container">
    <form action="<?php echo e(route('store_valuation')); ?>" method="POST">
        <?php echo csrf_field(); ?>
        <div class="row justify-content-center">
            <h1>Class <?php echo e($classs->class_name); ?> - Course <?php echo e($courses->course_name); ?></h1>
            <div class="col-md-12">
                <table id="tablee" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Grade</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($loop->iteration); ?></td>
                                <td><?php echo e($row->user_name); ?></td>
                                <td>
                                    <input type="number" name="grade[<?php echo e($row->user_id); ?>]" value="<?php echo e($valuations[$row->user_id]); ?>" min="0" max="100">
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">
                <input type="hidden" name="class_id" value="<?php echo e($classs->class_id); ?>">
                <input type="hidden" name="course_id" value="<?php echo e($courses->course_id); ?>">
                <input type="submit" class="btn btn-primary" name="submit" value="Submit">
                <a href="<?php echo e(url('valuation/pre/index/' . $classs->class_id)); ?>" class="btn btn-danger">Back</a>
            </div>
        </div>
    </form>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\_cns_sekolah\resources\views/valuation/input_valuation.blade.php ENDPATH**/ ?>