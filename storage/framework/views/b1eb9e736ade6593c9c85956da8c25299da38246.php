

<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <h1><?php echo e($users->user_name); ?>'s Study Card</h1>
        <div class="col-md-12">
            <table id="tablee" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Course</th>
                        <th>Grade</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($loop->iteration); ?></td>
                            <td><?php echo e($row->course_name); ?></td>
                            <td><?php echo e($valuations[$row->course_id]); ?></td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
        <?php if(Auth::user()->user_role != "Student"): ?>
            <div class="col-md-12">
                <a href="<?php echo e(url('valuation/pre/index_student')); ?>" class="btn btn-danger">Back</a>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\_cns_sekolah\resources\views/valuation/report_valuation_student.blade.php ENDPATH**/ ?>