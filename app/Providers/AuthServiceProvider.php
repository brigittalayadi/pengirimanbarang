<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies();

        $gate->define('admin',function($user){
            return $user->user_role == 'Admin';
        });

        $gate->define('student',function($user){
            return $user->user_role == 'Student';
        });

        $gate->define('teacher',function($user){
            return $user->user_role == 'Teacher';
        });
        
        $gate->define('headmaster',function($user){
            return $user->user_role == 'Headmaster';
        });
        
        $gate->define('library_staff',function($user){
            return $user->user_role == 'Library Staff';
        });
    }
}
